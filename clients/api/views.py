import re
from django.db import transaction
from django.utils import timezone
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from clients.models import Client, ClientContact
from .serializers import ClientSerializer
from .filters import ClientFilter
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
import pandas as pd


class ClientView(ModelViewSet):

    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filterset_class = ClientFilter

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset()).distinct()
        serializer = self.get_serializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class FileUploadView(APIView):
    parser_classes = [MultiPartParser]

    @swagger_auto_schema(
        operation_description='Upload "clients.csv" file. File can be generated by the "python manage.py gen_clients --500000".',
        operation_id='Upload csv',
        manual_parameters=[openapi.Parameter(
            name="file",
            in_=openapi.IN_FORM,
            type=openapi.TYPE_FILE,
            required=True,
            description="At this moment we accept generated client.csv file."
        )],
        responses={400: 'Invalid data in uploaded file',
                   200: 'Success'},
    )
    @transaction.atomic()
    def put(self, request):

        file_obj = request.data['file']

        df = pd.read_csv(file_obj, sep=',')

        row_iter = df.iterrows()

        start_time = timezone.now()

        # clients = [Client(name=row['Name']) for index, row in row_iter]
        # Client.objects.bulk_create(clients)

        for index, row in row_iter:
            client = Client.objects.create(name=row['Name'])
            contact_type = 1 if "@" in row['Contact'] else (2 if re.search('[a-zA-Z]', row['Contact']) else 0)
            ClientContact.objects.create(client=client, contact_type=contact_type, value=row['Contact'])

        end_time = timezone.now()

        print(end_time - start_time)

        return Response(status=204)