from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from .managers import UserManager
from django.utils import timezone


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email address', unique=True)
    first_name = models.CharField('first name', max_length=30, blank=True, null=True)
    last_name = models.CharField('last name', max_length=30, blank=True, null=True)
    date_joined = models.DateTimeField('date joined', auto_now_add=True)
    is_active = models.BooleanField('active', default=True)
    is_deleted = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(blank=True, null=True, verbose_name="Deleted date")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Updated date")
    date_of_birth = models.DateField(null=True, blank=True)

    objects = UserManager()
    all_objects = UserManager(alive_only=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        return True

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()

    def hard_delete(self):
        super().delete()

    def save(self, *args, **kwargs):

        existing = None

        if self.id:
            try:
                existing = type(self).all_objects.get(id=self.id)
            except:
                pass

        super().save()

        if (not existing and self.is_deleted) or (existing and not existing.is_deleted and self.is_deleted):
            self.deleted_at = timezone.now()
            super().save()

        if existing and existing.is_deleted and not self.is_deleted:
            self.deleted_at = None
            super().save()