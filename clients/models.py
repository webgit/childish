import uuid
from django.utils import timezone
from django.db import models
from .managers import SoftDeletionManager


class SoftUUIDModel(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created date")
    update_at = models.DateTimeField(auto_now=True, verbose_name="Updated date")
    deleted_at = models.DateTimeField(blank=True, null=True, verbose_name="Deleted date")

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True
        ordering = ['created_at']

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = timezone.now()
        super().save()

    def hard_delete(self):
        super().delete()


class Client(SoftUUIDModel):

    name = models.CharField(max_length=60, blank=True, null=True)


class ClientContact(SoftUUIDModel):

    class ContactType(models.IntegerChoices):
        MOB = 0, "MOBILE"
        EML = 1, "EMAIL"
        ADR = 2, "ADDRESS"

    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="client_contacts")
    contact_type = models.PositiveSmallIntegerField(choices=ContactType.choices, default=ContactType.MOB)
    value = models.CharField(max_length=30, blank=True, null=True)
