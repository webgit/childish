from django.contrib import admin
from users.models import User


@admin.register(User)
class UserProfileAdmin(admin.ModelAdmin):
    search_fields = ['email']
    list_display = ['id', 'email', 'date_joined', 'first_name', 'last_name', 'is_deleted']
    readonly_fields = ['deleted_at',]

    def get_queryset(self, request):
        return self.model.all_objects.all()
