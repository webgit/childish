from rest_framework import serializers
from clients.models import Client, ClientContact


class ClientContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = ClientContact
        fields = ('contact_type', 'value')


class ClientSerializer(serializers.ModelSerializer):

    client_contacts = ClientContactSerializer(many=True)

    class Meta:
        model = Client
        fields = ('id', 'name', 'client_contacts')

    def create(self, validated_data):
        client_contacts_data = validated_data.pop('client_contacts')
        client = Client.objects.create(**validated_data)
        ClientContact.objects.bulk_create(ClientContact(client=client, **i) for i in client_contacts_data)
        return client

    def update(self, instance, validated_data):
        client_contacts_data = validated_data.pop('client_contacts')
        ClientContact.objects.filter(client=instance).delete()
        ClientContact.objects.bulk_create(ClientContact(client=instance, **i) for i in client_contacts_data)
        updated = super().update(instance, validated_data)
        return updated



