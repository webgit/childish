from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import ClientView, FileUploadView

app_name = 'clients'

router = DefaultRouter(trailing_slash=False)
router.register(r'clients', ClientView, basename='clients')


urlpatterns = [
    path('', include(router.urls)),
    path('file-upload', FileUploadView.as_view())
]

urlpatterns += router.urls
