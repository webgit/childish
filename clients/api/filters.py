from django_filters import rest_framework as filters
from clients.models import Client


class ClientFilter(filters.FilterSet):
    class Meta:
        model = Client
        fields = {
            'id': ('exact', 'in'),
            'name': ('exact', 'in'),
            'client_contacts__contact_type': ('exact', 'in'),
            'client_contacts__value': ('exact', 'in'),
        }
