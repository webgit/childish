from random import choice
from django.utils import timezone
from django.core.management.base import BaseCommand, CommandError
import pandas as pd
from mimesis import Person, Address


class FakeData:

    person = Person()
    address = Address()

    def create_rows(self, qnt):

        output = [{"Name": self.person.full_name(),
                   "Contact": choice([self.person.email(),
                                      self.person.telephone(),
                                      self.address.address()]),
                   } for x in range(qnt)]
        return output


class Command(BaseCommand):

    help = 'Generates fake clients for tests.'

    def add_arguments(self, parser):
        parser.add_argument('--qnt', type=int)

    def handle(self, *args, **options):

        qnt = options['qnt']

        f = FakeData()

        start_time = timezone.now()
        df = pd.DataFrame(f.create_rows(qnt))
        end_time = timezone.now()
        time_delta = end_time - start_time
        print(time_delta)

        df.to_csv("clients.csv", encoding='utf-8', chunksize=2000)
        self.stdout.write(self.style.SUCCESS('Done'))

